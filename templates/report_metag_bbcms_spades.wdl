workflow jgi_meta_report {
    String out_root
    String bin_dir
    String conda_dir
    call reproduce {
    	 input: outroot=out_root
    }		 
    call report  {
    	 input: outroot=out_root
    }		 
    call post {
    	 input: outroot=out_root, metadata=report.out
    }		 

}

task reproduce {
     String outroot
     String outdir="reproduce"
     String outfile="reproduce.bash"
     String outpath="${outroot}/${outdir}/${outfile}"
     command{
		mkdir -p ${outroot}/${outdir}
		find ${outroot} -type f -name script | xargs head -100000 > ${outpath}
		cp stderr stdout script  ${outroot}/${outdir}
	}
     output {
     	    File out = outpath
     }
}

task report { 
     String outroot
     String outdir="report"
     String outfile="metadata.json"
     String outpath="${outroot}/${outdir}/${outfile}"
     command {
		mkdir -p ${outroot}/${outdir}
		/global/projectb/sandbox/gaag/bfoster/meta.dev/jgi-meta/bin/report_metag_bbcms_spades.py -i ${outroot} -j ${outroot}/setup.json -o ${outroot}/${outdir}
		cp stderr stdout script  ${outroot}/${outdir}
	 }
     output {
     	    File out = outpath
     }
}     

task post {
     File metadata
     String outroot
     String outdir="post"
     command {
     	mkdir -p ${outroot}/${outdir}
     	/global/projectb/sandbox/gaag/bfoster/meta.dev/jgi-meta/bin/post_meta_assembly.py -i ${outroot} -j ${metadata} -o ${outroot}/${outdir}
		cp stderr stdout script  ${outroot}/${outdir}     	
    }
}

