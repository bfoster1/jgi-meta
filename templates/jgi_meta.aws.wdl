workflow jgi_meta {
    File input_file
    Float unique_kmer
    String bbtools_container="bryce911/bbtools:38.44"
    String spades_container="bryce911/spades:3.13.0"
    String basic_container="bryce911/bbtools:38.44"
     #shooting for 30% occupancy based on: occupancy = (unique kmers)/((RAM bytes*0.7)*8/12)
    Int bbcms_mem_pred = ceil(( unique_kmer /0.138)/1000000000)

    call get_queue as bbcms_mem{
    	 input: request=bbcms_mem_pred, container=basic_container
    }
    call bbcms {
         input: infile=input_file, memfile=bbcms_mem.out, container=bbtools_container
    }
    call get_queue as spades_mem{
    	 input: request=bbcms.predicted_mem, container=basic_container 
    }
    call assy {
         input: infile1=bbcms.out1, infile2=bbcms.out2, memfile=spades_mem.out, container=spades_container
    }
    call create_agp {
         input: scaffolds_in=assy.out, container=bbtools_container
    }
    call get_queue as bbmap_mem{
    	 input: request=create_agp.bbmap_mem_pred, container=basic_container 
    }
    call read_mapping_pairs {
    	 input: reads=bbcms.out, ref=create_agp.outcontigs, memfile=bbmap_mem.out, container=bbtools_container
    }
output {
         File bbcms_out = bbcms.out
         File bbcms_out1 = bbcms.out1
         File bbcms_out2 = bbcms.out2
         File bbcms_stdout = bbcms.stdout
         File bbcms_stderr = bbcms.stderr
         File bbcms_counts = bbcms.outcounts
         File bbcms_kmer = bbcms.outkmer

         File spades_log = assy.outlog
         File spades_assy = assy.out

         File contigs = create_agp.outcontigs
         File scaffolds = create_agp.outscaffolds
         File agp = create_agp.outagp

         File bam = read_mapping_pairs.outbamfile
         File bamidx = read_mapping_pairs.outbamfileidx
         File sam = read_mapping_pairs.outsamfile
         File cov = read_mapping_pairs.outcovfile

         File bbcms_resource = bbcms.outresources
         File assy_resource = assy.outresources
         File agp_resource = create_agp.outresources
         File mapping_resource = read_mapping_pairs.outresources
    }
}


task read_mapping_pairs{
    File reads
    File ref
    File memfile   
    String container
  
    Map [String,String] map = read_json(memfile)

    String filename_resources="resources.log"
    String filename_unsorted="pairedMapped.bam"
    String filename_outsam="pairedMapped.sam.gz"
    String filename_sorted="pairedMapped_sorted.bam"
    String filename_sorted_idx="pairedMapped_sorted.bam.bai"
    String filename_bamscript="to_bam.sh"
    String filename_cov="covstats.txt"
    String dollar="$"
    #runtime { backend: "Local"}
     runtime {
             docker: container
             backend:  "genomics2-optimal-spot-ceq" 
             memory: map["mem"]
     }
    command{
        touch ${filename_resources};wget -q http://portal.nersc.gov/dna/metagenome/assembly/scripts/resources.bash && bash resources.bash ${filename_resources} &
        sleep 30
        bbmap.sh threads=${dollar}(grep "model name" /proc/cpuinfo | wc -l) ${map["java"]}  nodisk=true interleaved=true ambiguous=random in=${reads} ref=${ref} out=${filename_unsorted} covstats=${filename_cov} bamscript=${filename_bamscript}
	samtools sort -m100M -@ ${dollar}(grep "model name" /proc/cpuinfo | wc -l) ${filename_unsorted} -o ${filename_sorted}
        samtools index ${filename_sorted}
        reformat.sh threads=${dollar}(grep "model name" /proc/cpuinfo | wc -l) ${map["java"]} in=${filename_unsorted} out=${filename_outsam} overwrite=true
  }
  output{
      File outbamfile = filename_sorted
      File outbamfileidx = filename_sorted_idx
      File outcovfile = filename_cov
      File outsamfile = filename_outsam
      File outresources = filename_resources
  }
}

task create_agp {
    File scaffolds_in
    String container
    String java="-Xmx30g"
    String filename_resources="resources.log"
    String prefix="assembly"
    String filename_contigs="${prefix}.contigs.fasta"
    String filename_scaffolds="${prefix}.scaffolds.fasta"
    String filename_agp="${prefix}.agp"
    String filename_legend="${prefix}.scaffolds.legend"
#    runtime {backend: "Local"} 
     runtime {
             docker: container
             backend:  "genomics2-optimal-spot-ceq" 
             memory: "29.5 G"
     }
    command{
        touch ${filename_resources};wget -q http://portal.nersc.gov/dna/metagenome/assembly/scripts/resources.bash && bash resources.bash ${filename_resources} &
        sleep 30
        fungalrelease.sh ${java} in=${scaffolds_in} out=${filename_scaffolds} outc=${filename_contigs} agp=${filename_agp} legend=${filename_legend} mincontig=200 minscaf=200 sortscaffolds=t sortcontigs=t overwrite=t
  }
    output{
	File outcontigs = filename_contigs
	File outscaffolds = filename_scaffolds
	File outagp = filename_agp
    	File outlegend = filename_legend
    	Int bbmap_mem_pred = ceil((size(filename_contigs) * 7)/1000000000 + 0.5) # 7 bytes per reference base + 400 byes/contig 
    	File outresources = filename_resources
    }
}

task assy {
     File infile1
     File infile2
     File memfile
     String container

     Map [String,String] map = read_json(memfile)
     String filename_resources="resources.log"
     String outprefix="spades3"
     String filename_outfile="${outprefix}/scaffolds.fasta"
     String filename_spadeslog ="${outprefix}/spades.log"
     String dollar="$"
#     runtime {backend: "Local"}
     runtime {
             docker: container
             backend:  "genomics2-optimal-spot-ceq" 
             memory: map["mem"]
     }
     command{
        echo ${map["java"]};
        touch ${filename_resources};wget -q http://portal.nersc.gov/dna/metagenome/assembly/scripts/resources.bash && bash resources.bash ${filename_resources} &
        sleep 30
        spades.py -m ${map["mem_safe"]} -o ${outprefix} --only-assembler -k 33,55,77,99,127  --meta -t ${dollar}(grep "model name" /proc/cpuinfo | wc -l) -1 ${infile1} -2 ${infile2}
     }
     output {
            File out = filename_outfile
            File outlog = filename_spadeslog
            File outresources = filename_resources
     }
}

task bbcms {
     File infile
     File memfile
     String container

     Map [String,String] map = read_json(memfile)
     String filename_resources="resources.log"
     String filename_outfile="input.corr.fastq.gz"
     String filename_outfile1="input.corr.left.fastq.gz"
     String filename_outfile2="input.corr.right.fastq.gz"
     String filename_readlen="readlen.txt"
     String filename_outlog="stdout.log"
     String filename_errlog="stderr.log"
     String filename_kmerfile="unique31mer.txt"
     String filename_counts="counts.metadata.json"
     String dollar="$"
#     runtime { backend: "Local"} 
     runtime {
             docker: container
             backend:  "genomics2-optimal-spot-ceq" 
             memory: map["mem"]
     }
     command {
        touch ${filename_resources};wget -q http://portal.nersc.gov/dna/metagenome/assembly/scripts/resources.bash && bash resources.bash ${filename_resources} &
        sleep 30
        bbcms.sh threads=${dollar}(grep "model name" /proc/cpuinfo | wc -l) ${map["java"]} metadatafile=${filename_counts} mincount=2 highcountfraction=0.6 in=${infile} out=${filename_outfile} > >(tee -a ${filename_outlog}) 2> >(tee -a ${filename_errlog} >&2) && grep Unique ${filename_errlog} | rev |  cut -f 1 | rev  > ${filename_kmerfile}
        reformat.sh threads=${dollar}(grep "model name" /proc/cpuinfo | wc -l) in=${filename_outfile} out1=${filename_outfile1} out2=${filename_outfile2}
        readlength.sh in=${filename_outfile} out=${filename_readlen}
     }
     output {
            File out = filename_outfile
            File out1 = filename_outfile1
            File out2 = filename_outfile2
            File outreadlen = filename_readlen
            File stdout = filename_outlog
            File stderr = filename_errlog
            File outcounts = filename_counts
            File outkmer = filename_kmerfile
            Int predicted_mem = ceil((read_float(filename_kmerfile) * 0.00000002962 + 16.3) * 1.075 )
            File outresources = filename_resources
     }
}

task get_queue {
    String container
    String request
    String outfile="mem.json"
#    runtime {backend: "Local"} 
     runtime {
             docker: container
             backend: "genomics2-optimal-spot-ceq"
             memory: "29.5 G"
     }
    command {
python <<CODE
import sys,json
request = int(${request})
bins = [30,60,120,160,240,480]
val = [i for i in bins if request < i][0]
out=dict()
out['request']=request
#out['queue'] = str(val).zfill(3) + "g-spot-ceq"
out['mem'] = float(val) - 0.5
out['mem_safe'] = int(out['mem'] * 0.95)
out['mem'] = str(out['mem']) + " G"
out['java'] = '-Xmx' + str(out['mem_safe']) + 'g'
with open("${outfile}","w") as f:f.write(json.dumps(out,indent=3))
CODE
    }
    output {
           File out = outfile
    } 
}     
