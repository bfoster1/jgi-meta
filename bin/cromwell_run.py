#!/usr/bin/env python

import sys, os
#sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "..","lib"))
import argparse
import json
import subprocess
import shutil
import re
import time
import hashlib

#requires an active aws cli if using s3
'''
args
- s3 or local staging dir with default
- time check argument
- outputdir for final files
- send only flag
'''
if sys.version_info[0] <3:
    sys.stderr.write('use python3' + "\n")
    sys.exit(1)
if shutil.which('aws') is None:
    sys.stderr.write('awscli not in path or not installed' + "\n")
    sys.exit(1)

VERSION = "1.0.1"
def main():
    '''given a reads inputfile, and a wdl, copy reads to s3 and launch cromwell job 
    wait for completion and copy outputs back
    '''

    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument("-j", "--inputsjson", required=True, help="inputs.json for cromwell submission. Must match wdl.\n")
    parser.add_argument("-w", "--wdl", required=True, help="wdl file for cromwell.\n")
    parser.add_argument("-s", "--s3", required=False,default='s3://bf-20190529-uswest2-s3/cromwell-execution', help="s3 destination for input file/n")
    parser.add_argument("-c", "--cromwellhost", required=False,default="127.0.0.1:8000",  help="cromwell host to push job to\n")

    parser.add_argument("-f", "--finalize", required=False, default=False, action='store_true', help="add reports, metadata.json after cp\n")
    parser.add_argument("-t", "--time", required=False, default=30, help="time in seconds to monitor the directory. (default=60)\n")
    parser.add_argument("-o", "--outputdir", required=False, default = './', help="outputdir for outputs. default=send to cromwell only\n")
    parser.add_argument("--verbose", required=False, default=False,action='store_true', help="verbose mode")
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    args = parser.parse_args()

        
    if not args.s3.startswith('s3://'):
        sys.stderr.write(args.s3 + " does not look like s3://\n")
        sys.exit(1)
    if args.outputdir and not os.path.exists(args.outputdir):
        sys.stderr.write(args.outputdir + " does not exist \n")
        sys.exit(255)
    args.outputdir == os.path.abspath(args.outputdir)

    ###########################
#    import pdb;pdb.set_trace()
#    metadatafile = "/global/cscratch1/sd/bfoster/metag/whole/test_final/dc0ecb450982129eba41c7fd899bc6aa_._cromwell.metadata"
#    with open(metadatafile,"r") as f:metadata = json.loads(f.read())
#    ts = '1565977688.534611'
###########################
    
    cmd = ['curl', '-Is', '--max-time', '10', 'http://' + args.cromwellhost ]
    result = run_process(cmd,verbose=args.verbose)

    #get workflow name frow wdl
    workflow = ""
    with open(args.wdl,"r") as f:
        for line in f.readlines():
            if re.match(r'^\s*workflow', line) is not None:
                workflow = line.split()[1]
                workflow = re.sub(r"{",r'',workflow)
                workflow = re.sub(r'\s',r'',workflow)

    with open(args.inputsjson, "r") as f:inputs_json = json.loads(f.read())
    inputs_tag = workflow + ".input_file"
    inputs_file_local = inputs_json[inputs_tag]

    md5 = ""
    cmd = ["md5sum",  inputs_file_local]
    result = run_process(cmd,verbose=args.verbose)
    md5 = result['stdout']
    if args.verbose: sys.stdout.write("md5sum: " + md5 + "\n");sys.stdout.flush()
    md5 = md5.split()[0]
    ts = md5 + "_._"

    inputs_file_s3 = os.path.join(args.s3,ts + os.path.basename(inputs_file_local))
    inputs_json[inputs_tag] = inputs_file_s3
    final_inputs_json = os.path.join(args.outputdir, ts + "inputs.json")
    with open(final_inputs_json,"w") as f:f.write(json.dumps(inputs_json,indent=3))

    cmd = ["aws","s3", "cp", inputs_file_local, inputs_file_s3]
    result = run_process(cmd,verbose=args.verbose)

    cmd = 'curl --silent -X POST "http://' + args.cromwellhost + '/api/workflows/v1" -H  "accept: application/json" -H  "Content-Type: multipart/form-data" '
    cmd += '-F "workflowSource=@' + args.wdl + ';" ' 
    cmd += '-F "workflowInputs=@' + final_inputs_json + ';type=application/json" '
    #    cmd += '-F "workflowOptions=@options.json;type=application/json" '
    result = run_process(cmd,verbose=args.verbose)
    submission = json.loads(result['stdout'])
    sub_id = submission['id']
    if args.verbose: sys.stdout.write("cromwell_id: " + sub_id + "\n");sys.stdout.flush()    

    cmd = 'curl --silent -X GET "http://' + args.cromwellhost + '/api/workflows/v1/' + sub_id + '/metadata?expandSubWorkflows=false" -H  "accept: application/json"'
    if args.verbose: sys.stdout.write("cromwell_check: " + cmd + "\n");sys.stdout.flush()    
    metadatafile = os.path.join(args.outputdir,ts + "cromwell.metadata")
    ### Start submission loop
    finished = False
    while not finished:
        time.sleep(int(args.time))
        result = run_process(cmd,verbose=False)
        metadata = json.loads(result['stdout'])
        if args.verbose: sys.stdout.write("cromwell_status: " + metadata['status'] + "\n");sys.stdout.flush()    
        with open(metadatafile,"w") as f:
            f.write(json.dumps(metadata,indent=3))
        #['Running', 'Succeeded', 'Aborted', 'Submitted', 'On Hold', 'Failed']
        if metadata['status'] not in ['Submitted', 'Running', 'On Hold']:
            finished = True

    if metadata['status'] in ['Aborted', 'Failed']:
        sys.stderr.write("Error, check on: " + metadata['id'] + "\n")
        sys.exit(1)

    dirs = dict()
    if len(metadata['outputs']):
        dirs = copy_outputs(metadata,ts, args.outputdir, args.verbose)
        cmd = ["aws","s3", "rm", dirs['s3_dir'], "--recursive"]
        result = run_process(cmd,verbose=args.verbose)        
    else:
        sys.stderr.write("Error no outputs found in cromwell metadata")
        sys.exit(1)

    cmd = ["aws","s3", "rm", inputs_file_s3]
    result = run_process(cmd,verbose=args.verbose)

    if args.finalize:
        finalize_bash = os.path.join(os.path.dirname(__file__),"jgi_meta_finalize.bash")
        cmd = [finalize_bash, dirs['local_dir']]
        result = run_process(cmd,verbose=args.verbose)                    
        if args.verbose:sys.stdout.write(" ".join(cmd) + "\n")
        out = subprocess.Popen(cmd,stderr=subprocess.STDOUT,stdout=subprocess.PIPE)
        result = {"output":out.communicate(), "rc": out.returncode}
        if result['rc']  == 0:#successful
            target_dir = dirs['local_dir']
            for name in os.listdir(target_dir):
                shutil.move(os.path.join(target_dir, name), os.path.join(os.path.dirname(target_dir), name))
            os.rmdir(target_dir)
        else:
            sys.stderr.write("Finalize command did not complete ",finalize_bash)
            sys.exit()
        #move up one dir

    sys.stdout.write("Done\n")
    sys.exit()
        

def copy_outputs(metadata, prefix, localdir, verbose=False):
    '''takes cromwell metadata, finds the first s3 output and cps it locally with prefix                                                                                                                 if successfull, returns the local dir and s3 dir for deletion'''

    s3_dir = ""
    cromwellid = metadata['id']
    localdir_root= os.path.join(localdir,prefix + cromwellid)

    #to add, check for outputs. If not, then get source path from "calls"                                                                                                                              
    try:
        for key in sorted(metadata['outputs']):
            if s3_dir:
                continue
            source_path = metadata['outputs'][key]
            print (str(source_path))
            if not str(source_path).startswith('s3://'):
                continue
            if source_path.find('/call-') != -1 and source_path.find('/cromwell-execution/') != -1:
                s3_dir = re.sub(r'^(.*?/)call-.*$',r'\1',source_path)
            cmd = ["aws","s3", "cp", s3_dir, localdir_root, "--recursive"]
            if verbose:sys.stdout.write(" ".join(cmd))
            procExe = subprocess.Popen(cmd , stdout=subprocess.PIPE, stderr=subprocess.PIPE,universal_newlines=True)
            while procExe.poll() is None:
                line = procExe.stdout.readline()
                if verbose and line.startswith("download:"):
                    sys.stdout.write(line)
    except Exception as e:
        sys.stderr.write(e)
        return
    return {"s3_dir":s3_dir,"local_dir":localdir_root}

def run_process(cmd, verbose=False, fail=True):
    cmd_string=""
    if isinstance(cmd,list):
        if verbose:
            sys.stdout.write("call:" + " ".join(cmd) + " ")
        out = subprocess.Popen(cmd,stderr=subprocess.STDOUT,stdout=subprocess.PIPE)
        cmd_string = " ".join(cmd)
    elif isinstance(cmd,str):
        if verbose:
            sys.stdout.write("call:" + cmd + " ")
        out = subprocess.Popen(cmd,shell=True,stderr=subprocess.STDOUT,stdout=subprocess.PIPE)
        cmd_string = cmd
    else:
        sys.stderr.write("trouble running:" + str(cmd), "\n")
        sys.exit(1)
    comm = out.communicate()
    stdout = comm[0].decode('utf-8') if comm[0] is not None else ""
    stderr = comm[1].decode('utf-8') if comm[1] is not None else ""
    rc = out.returncode
    if rc !=0 and fail:
        sys.stderr.write("Trouble running: \"" + cmd_string + "\"\n")
        sys.stderr.write("Error code:" + str(rc) + "\n")
        sys.exit()
    elif verbose:
        sys.stdout.write(" ... returned: " + str(rc) + "\n")
    return_vals = {"stdout":stdout, "stderr":stderr, "rc": rc, "cmd": cmd}
    return return_vals

if __name__ == '__main__':
    main()
