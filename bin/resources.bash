#!/usr/bin/env bash

#ulimit -a
#
which ifconfig &> /dev/null && ifconfig eth0 | sed -nre '/^[^ ]+/{N;s/^([^ ]+).*inet addr:*([^ ]+).*/\2/p}' 
grep "model name" /proc/cpuinfo |  sort | uniq -c 
(curl --fail --max-time 10 --silent http://169.254.169.254/latest/meta-data/instance-id;echo)
(curl --fail --max-time 10 --silent http://169.254.169.254/latest/meta-data/instance-type;echo)
#(curl --fail --max-time 10 --silent http://169.254.169.254/latest/meta-data/iam/info;echo) | tee -a $log;
#(curl --fail --max-time 10 --silent http://169.254.169.254/latest/meta-data/ami-launch-index;echo) | tee -a $log;
while true; 
    do echo $(date)"__"$(grep Mem /proc/meminfo)"__"$(cat /proc/loadavg)"__"$(ps -eo pcpu | grep -v CPU |  sort -k 1 -nr | head -5 | xargs | sed s/\\s/\\,/g)
    sleep 30; 
done
